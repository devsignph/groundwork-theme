<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Groundwork
 * @since 1.0.0
 */

get_header();
?>
<div class="container">
    <div class="row">
        <div class="col-12 col-lg-7">
            <?php
            if ( have_posts() ) :
                while ( have_posts() ) :
                    the_post();
                    ?>
                    <h1><?php the_title(); ?></h1>
                    <p><?php the_content(); ?></p>
                    <?php
                endwhile;
            endif;
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;
            ?>
        </div>
    </div>
</div>
<?php
get_footer();
