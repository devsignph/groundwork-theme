<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Groundwork
 * @since 1.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <div id="app" class="wrapper">
        <?php if ( ! is_404() ) : ?>
            <?php if ( carbon_get_theme_option( 'ds_header_top' ) ) : ?>
                <div class="header-top">
                    <div class="container">
                        <ul>
                            <li><?php echo esc_html( do_shortcode( '[ds_phone]' ) ); ?></li>
                            <li><?php echo esc_html( do_shortcode( '[ds_email]' ) ); ?></li>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
            <nav class="navbar navbar-expand-lg">
                <div class="container">
                    <a class="navbar-brand" href="<?php echo esc_url( home_url() ); ?>">
                        <?php echo bloginfo( 'blogname' ); ?>
                    </a>
                    <button class="hamburger hamburger--slider navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Dropdown
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="<?php echo esc_url( home_url( '/' ) ); ?>">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo esc_url( home_url( '/about' ) ); ?>">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav-link--cta" href="<?php echo esc_url( home_url( '/contact' ) ); ?>">Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <main>
        <?php endif; ?>
