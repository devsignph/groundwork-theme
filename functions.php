<?php
/**
 * Devsign functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Groundwork
 * @since 1.0.0
 */

require_once 'inc/devsign-global-variables.php';
require_once 'inc/devsign-theme-options.php';
require_once 'inc/devsign-custom-post-types.php';
require_once 'inc/devsign-custom-fields.php';
require_once 'inc/devsign-filters.php';
require_once 'inc/devsign-widgets.php';
require_once 'inc/devsign-shortcodes.php';
require_once 'inc/devsign-wp-login.php';

/** Load Devsign styles and scripts. */
function gw_files() {
    wp_enqueue_style( 'gw_main_styles', get_template_directory_uri() . '/public/css/app.css', null, '1.0' );
    wp_enqueue_script( 'gw_main_scripts', get_stylesheet_directory_uri() . '/public/js/app.js', null, '1.0', true );
}

add_action( 'wp_enqueue_scripts', 'gw_files' );

/** Activate some WordPress features. */
function gw_features() {
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );

    add_image_size( 'gallery-thumbnail', 250, 250, true );
}

add_action( 'after_setup_theme', 'gw_features' );

/** Hide the WordPress Version */
function gw_remove_version() {
    return '';
}

add_filter( 'the_generator', 'gw_remove_version' );

/** Remove Emoji for better speed optimization */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

/** Add An Environment Watermark */
function gw_env_watermark() {
    if ( defined( 'WP_ENV' ) ) {
        echo '<div class="watermark">' . esc_html( WP_ENV ) . '</div>';
    }
}

add_action( 'wp_footer', 'gw_env_watermark' );
