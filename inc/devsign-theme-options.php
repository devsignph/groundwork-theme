<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

/** Add 'Theme Options' capability in WordPress Admin */
function gw_attach_theme_options() {
    global $theme_options;
    global $header_options;
    global $contact_info_options;
    global $social_links_options;

    $theme_options = Container::make( 'theme_options', 'Theme Options' );

    $header_options = Container::make( 'theme_options', 'Header' )
        ->set_page_parent( $theme_options )
        ->add_fields(
            array(
                Field::make( 'radio', 'gw_header_top', 'Show Header Top' )
                    ->set_options(
                        array(
                            1 => 'Yes',
                            0 => 'No',
                        )
                    ),
                Field::make( 'radio', 'gw_sticky_header', 'Sticky Header' )
                    ->set_options(
                        array(
                            1 => 'Yes',
                            0 => 'No',
                        )
                    ),
            )
        );

    $contact_info_options = Container::make( 'theme_options', 'Contact Info' )
        ->set_page_parent( $theme_options )
        ->add_fields(
            array(
                Field::make( 'text', 'gw_address', 'Address' ),
                Field::make( 'text', 'gw_phone', 'Phone/Mobile' ),
                Field::make( 'text', 'gw_email', 'Email' ),
            )
        );

    $social_links_options = Container::make( 'theme_options', 'Social Links' )
        ->set_page_parent( $theme_options )
        ->add_fields(
            array(
                Field::make( 'text', 'gw_facebook', 'Facebook' ),
                Field::make( 'text', 'gw_twitter', 'Twitter' ),
                Field::make( 'text', 'gw_instagram', 'Instagram' ),
                Field::make( 'text', 'gw_linkedin', 'LinkedIn' ),
                Field::make( 'text', 'gw_pinterest', 'Pinterest' ),
            )
        );
}

add_action( 'carbon_fields_register_fields', 'gw_attach_theme_options' );
