<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

/** Register custom fields */
function gw_attach_custom_fields() {
    /** Banner */
    Container::make( 'post_meta', 'Banner' )
        ->where( 'post_type', '=', 'page' )
        ->add_fields(
            array(
                Field::make( 'text', 'gw_banner_heading', 'Heading' ),
                Field::make( 'text', 'gw_banner_subheading', 'Sub Heading' ),
                Field::make( 'rich_text', 'gw_banner_text', 'Text' ),
                Field::make( 'text', 'gw_banner_primary_button_text', 'Primary Button Text' )
                    ->set_attribute( 'placeholder', 'e.g. Contact Us' ),
                Field::make( 'text', 'gw_banner_primary_button_link', 'Primary Button Link' )
                    ->set_attribute( 'placeholder', 'e.g. /contact' ),
                Field::make( 'text', 'gw_banner_secondary_button_text', 'Secondary Button Text' )
                    ->set_attribute( 'placeholder', 'e.g. Learn More' ),
                Field::make( 'text', 'gw_banner_secondary_button_link', 'Secondary Button Text' )
                    ->set_attribute( 'placeholder', 'e.g. /about' ),
            )
        );

    /** Contact Page */
    Container::make( 'post_meta', 'Google Map' )
        ->where( 'post_type', '=', 'page' )
        ->where( 'post_template', '=', 'page-templates/page-contact.php' )
        ->add_fields(
            array(
                Field::make( 'text', 'gw_google_map', 'HTML Code' ),
            )
        );

    /** Testimonial */
    Container::make( 'post_meta', 'Author' )
        ->where( 'post_type', '=', 'testimonial' )
        ->add_fields(
            array(
                Field::make( 'image', 'gw_testimonial_author_image', 'Image' ),
                Field::make( 'text', 'gw_testimonial_author_name', 'Name' )
                    ->set_attribute( 'placeholder', 'e.g. John Doe' )
                    ->set_required( true ),
                Field::make( 'text', 'gw_testimonial_author_profession', 'Profession' )
                    ->set_attribute( 'placeholder', 'e.g. CEO' ),
                Field::make( 'text', 'gw_testimonial_author_company', 'Company' )
                    ->set_attribute( 'placeholder', 'e.g. XYZ Company' ),
            )
        );

    /** Gallery */
    Container::make( 'post_meta', 'Images' )
        ->where( 'post_type', '=', 'gallery' )
        ->add_fields(
            array(
                Field::make( 'media_gallery', 'gw_gallery_media', 'Upload Images' ),
            )
        );
}

add_action( 'carbon_fields_register_fields', 'gw_attach_custom_fields' );

/** Load carbon fields */
function gw_crb_load() {
    require_once ABSPATH . 'vendor/htmlburger/carbon-fields/core/Carbon_Fields.php';

    \Carbon_Fields\Carbon_Fields::boot();
}

add_action( 'after_setup_theme', 'gw_crb_load' );
