<?php

/** Add class to the Body tag */
function gw_body_class( $classes ) {
    if ( carbon_get_theme_option( 'gw_sticky_header' ) ) {
        $classes[] = 'header--sticky';
    }

    if ( is_page_template( 'page-templates/page-home.php' ) ) {
        $classes[] = 'page-home';
    }

    if ( is_page_template( 'page-templates/page-contact.php' ) ) {
        $classes[] = 'page-contact';
    }

    if ( is_page_template( 'page-templates/page-about.php' ) ) {
        $classes[] = 'page-about';
    }

    if ( is_page_template( 'page-templates/page-gallery.php' ) ) {
        $classes[] = 'page-gallery';
    }

    if ( is_page_template( 'page-templates/page-privacy-policy.php' ) ) {
        $classes[] = 'page-privacy-policy';
    }

    return $classes;
}

add_filter( 'body_class', 'gw_body_class' );
