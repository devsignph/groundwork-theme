<?php

/** Facebook Shortcode */
function gw_facebook_shortcode() {
    return carbon_get_theme_option( 'gw_facebook' );
}

add_shortcode( 'gw_facebook', 'gw_facebook_shortcode' );

/** Twitter Shortcode */
function gw_twitter_shortcode() {
    return carbon_get_theme_option( 'gw_twitter' );
}

add_shortcode( 'gw_twitter', 'gw_twitter_shortcode' );

/** Instagram Shortcode */
function gw_instagram_shortcode() {
    return carbon_get_theme_option( 'gw_instagram' );
}

add_shortcode( 'gw_instagram', 'gw_instagram_shortcode' );

/** LinkedIn Shortcode */
function gw_linkedin_shortcode() {
    return carbon_get_theme_option( 'gw_linkedin' );
}

add_shortcode( 'gw_linkedin', 'gw_linkedin_shortcode' );

/** Pinterest Shortcode */
function gw_pinterest_shortcode() {
    return carbon_get_theme_option( 'gw_pinterest' );
}

add_shortcode( 'gw_pinterest', 'gw_pinterest_shortcode' );

/** Address Shortcode */
function gw_address_shortcode() {
    return carbon_get_theme_option( 'gw_address' );
}

add_shortcode( 'gw_address', 'gw_address_shortcode' );

/** Phone Shortcode */
function gw_phone_shortcode() {
    return carbon_get_theme_option( 'gw_phone' );
}

add_shortcode( 'gw_phone', 'gw_phone_shortcode' );

/** Email Shortcode */
function gw_email_shortcode() {
    return carbon_get_theme_option( 'gw_email' );
}

add_shortcode( 'gw_email', 'gw_email_shortcode' );
