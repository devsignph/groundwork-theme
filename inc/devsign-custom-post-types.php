<?php

/** Register custom post types */
function gw_post_types() {
    register_post_type(
        'testimonial',
        array(
            // 'capability_type' => 'testimonial',
            // 'map_meta_cap' => true,
            'supports' => array(
                'title',
                'editor',
            ),
            'rewrite' => array(
                'slug' => 'testimonials',
            ),
            'has_archive' => true,
            'public' => true,
            'labels' => array(
                'name' => 'Testimonials',
                'add_new_item' => 'Add New Testimonial',
                'edit_item' => 'Edit Testimonial',
                'all_items' => 'All Testimonials',
                'singular_name' => 'Testimonial',
            ),
            'menu_icon' => 'dashicons-format-status',
        )
    );

    register_post_type(
        'gallery',
        array(
            // 'capability_type' => 'gallery',
            // 'map_meta_cap' => true,
            'supports' => array(
                'title',
                'editor',
            ),
            'rewrite' => array(
                'slug' => 'gallery',
            ),
            'has_archive' => false,
            'public' => true,
            'labels' => array(
                'name' => 'Galleries',
                'add_new_item' => 'Add New Gallery',
                'edit_item' => 'Edit Gallery',
                'all_items' => 'All Galleries',
                'singular_name' => 'Gallery',
            ),
            'menu_icon' => 'dashicons-images-alt2',
        )
    );
}

add_action( 'init', 'gw_post_types' );
