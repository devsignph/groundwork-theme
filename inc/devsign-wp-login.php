<?php

/** Customize the Login Screen */
function ds_header_url() {
    return esc_url( home_url( '/' ) );
}

add_filter( 'login_headerurl', 'ds_header_url' );

/** Customize the CSS of the Login Screen */
function ds_login_css() {
    //
}

add_action( 'login_enqueue_scripts', 'ds_login_css' );

/** Customize the Name */
function ds_login_title() {
    return get_bloginfo( 'name' );
}

add_filter( 'login_headertext', 'ds_login_title' );
