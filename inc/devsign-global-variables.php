<?php
/**
 * Devsign Global Functions
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Devsign Base Template
 * @since 1.0.0
 */

/**
 * Theme Options Container.
 * Use it in the child theme inside set_parent_page()
 * function to add additional submenus.
 */
$theme_options;

/**
 * Theme Options Submenus Container.
 * Use it in the child theme by calling add_fields() to
 * any of these to add additional fields.
 */
$header_options;
$contact_info_options;
$social_links_options;
