<?php

if ( function_exists('register_sidebar') ) {
    register_sidebar(
        array(
            'id' => 'footer-column-1',
            'name' => 'Footer Column 1',
            'before_widget' => '<div class = "footer-widget">',
            'after_widget' => '</div>',
            'before_title' => '<p class="footer-widget-title">',
            'after_title' => '</p>',
        )
    );

    register_sidebar(
        array(
            'id' => 'footer-column-2',
            'name' => 'Footer Column 2',
            'before_widget' => '<div class = "footer-widget">',
            'after_widget' => '</div>',
            'before_title' => '<p class="footer-widget-title">',
            'after_title' => '</p>',
        )
    );

    register_sidebar(
        array(
            'id' => 'footer-column-3',
            'name' => 'Footer Column 3',
            'before_widget' => '<div class = "footer-widget">',
            'after_widget' => '</div>',
            'before_title' => '<p class="footer-widget-title">',
            'after_title' => '</p>',
        )
    );

    register_sidebar(
        array(
            'id' => 'footer-column-4',
            'name' => 'Footer Column 4',
            'before_widget' => '<div class = "footer-widget">',
            'after_widget' => '</div>',
            'before_title' => '<p class="footer-widget-title">',
            'after_title' => '</p>',
        )
    );
}
