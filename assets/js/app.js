import 'wow.js';
import 'popper.js';
import $ from 'jquery';
import 'bootstrap';
import 'slick-carousel';
import Chocolat from 'chocolat';

$(document).ready(function () {
    $.fn.Chocolat = Chocolat;

    $('.hamburger').on('click', function () {
        $(this).toggleClass('is-active');
    });

    $('.gallery-media').Chocolat();
});
