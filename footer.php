<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Groundwork
 * @since 1.0.0
 */
?>
<?php if ( ! is_404() ) : ?>
            </main> <!-- End of Main tag -->
            <?php get_template_part( 'template-parts/footer' ); ?>
        </div> <!-- End of Wrapper class -->
        <?php wp_footer(); ?>
    </body>
</html>
<?php endif; ?>
