<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Groundwork
 * @since 1.0.0
 */
get_header();
?>
<div class="container">
    <h1>404.</h1>
    <p>The page you are looking for couldn't be found.</p>
    <p>
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="btn btn-primary">
            Go to Home
        </a>
    </p>
</div>
<?php
get_footer();
