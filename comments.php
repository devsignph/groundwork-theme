<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Groundwork
 * @since 1.0.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
*/
if ( post_password_required() ) {
    return;
}
?>

<div id="comments" class="comments-area">
    <?php
    if ( have_comments() ) :
        ?>
        <ol class="comment-list">
            <?php
            $args = [
                'walker' => null,
                'max_depth' => 4,
                'style' => 'ul',
                'callback' => null,
                'end-callback' => null,
                'type' => 'all',
                'reply_text' => 'Reply',
                'page' => '',
                'per_page' => '',
                'avatar_size' => 32,
                'reverse_top_level' => null,
                'reverse_children' => '',
                'format' => 'html5',
                'short_ping' => false,
                'echo' => true,
            ];

            wp_list_comments( $args );
            ?>
        </ol>
        <?php
        if ( ! comments_open() && get_comments_number() ) :
            ?>
            <p class="no-comments">Comments are closed.</p>
            <?php
        endif;
    endif;
    comment_form();
    ?>
</div>
