<div class="banner banner--center" style="background-image: url(<?php echo esc_url( get_the_post_thumbnail_url( get_the_ID() ) ); ?>)">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="banner-heading"><?php echo esc_html( carbon_get_the_post_meta( 'ds_banner_heading' ) ?: get_the_title() ); ?></h1>
                <?php if ( carbon_get_the_post_meta( 'ds_banner_subheading' ) ) : ?>
                <p class="banner-subheading"><?php echo esc_html( carbon_get_the_post_meta( 'ds_banner_subheading' ) ); ?></p>
                <?php endif; ?>
                <?php if ( carbon_get_the_post_meta( 'ds_banner_text' ) ) : ?>
                <div class="banner-text"><?php echo wpautop( carbon_get_the_post_meta( 'ds_banner_text' ) ); ?></div>
                <?php endif; ?>
                <?php if ( carbon_get_the_post_meta( 'ds_banner_primary_button_link' ) && carbon_get_the_post_meta( 'ds_banner_primary_button_text' ) ) : ?>
                <div class="banner-call-to-action">
                    <?php if ( carbon_get_the_post_meta( 'ds_banner_primary_button_link' ) && carbon_get_the_post_meta( 'ds_banner_primary_button_text' ) ) : ?>
                        <a href="<?php echo esc_url( home_url( carbon_get_the_post_meta( 'ds_banner_primary_button_link' ) ) ); ?>" class="btn btn-primary">
                            <?php echo esc_html( carbon_get_the_post_meta( 'ds_banner_primary_button_text' ) ); ?>
                        </a>
                    <?php endif; ?>
                    <?php if ( carbon_get_the_post_meta( 'ds_banner_secondary_button_link' ) && carbon_get_the_post_meta( 'ds_banner_secondary_button_text' ) ) : ?>
                        <a href="<?php echo esc_url( home_url( carbon_get_the_post_meta( 'ds_banner_secondary_button_link' ) ) ); ?>" class="btn btn-secondary">
                            <?php echo esc_html( carbon_get_the_post_meta( 'ds_banner_secondary_button_text' ) ); ?>
                        </a>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
