<footer class="footer">
    <div class="footer-information-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <?php dynamic_sidebar( 'Footer Column 1' ); ?>
                </div>
                <div class="col-lg-3">
                    <?php dynamic_sidebar( 'Footer Column 2' ); ?>
                </div>
                <div class="col-lg-3">
                    <?php dynamic_sidebar( 'Footer Column 3' ); ?>
                </div>
                <div class="col-lg-3">
                    <?php dynamic_sidebar( 'Footer Column 4' ); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p class="footer-copyright">Copyright <?php echo esc_html( date( 'Y' ) ); ?>. <?php echo bloginfo( 'blogname' ); ?>. All Rights Reserved. <a href="">Website</a> by <a href="">Paolo Yumul</a>.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
