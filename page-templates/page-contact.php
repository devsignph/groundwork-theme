<?php
/**
* Template Name: Contact Page
* The template for displaying the Contact page.
*
* @package WordPress
* @subpackage Devsign Base Template
*/

get_header();
?>
<?php get_template_part( 'template-parts/banner' ); ?>
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
            </div>
            <div class="col-md-6">
                <h2>Business Address</h2>
                <p><?php echo esc_html( do_shortcode( '[ds_address]' ) ); ?></p>
                <h2>Email</h2>
                <p><?php echo esc_html( do_shortcode( '[ds_email]' ) ); ?></p>
                <h2>Phone</h2>
                <p><?php echo esc_html( do_shortcode( '[ds_phone]' ) ); ?></p>
            </div>
        </div>
    </div>
</section>
<div class="google-map">
    <?php echo carbon_get_the_post_meta( 'ds_google_map' ) ?>
</div>
<?php
get_footer();
