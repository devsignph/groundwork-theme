<?php
/**
* Template Name: Gallery Page
* The template for displaying the Gallery page.
*
* @package WordPress
* @subpackage Devsign Base Template
*/

get_header();
?>
<section class="section">
    <div class="container">
        <div class="gallery-media" data-chocolat-title="Gallery Media">
            <?php
            $galleries = new WP_Query(
                array(
                    'post_type' => 'gallery',
                    'posts_per_page' => -1,
                    'order' => 'ASC',
                )
            );
            while ( $galleries->have_posts() ) :
                $galleries->the_post();
                foreach ( carbon_get_post_meta( get_the_ID(), 'gw_gallery_media' ) as $medium ) :
                    $full_size = wp_get_attachment_url( $medium );
                    ?>
                    <a class="chocolat-image" href="<?php echo esc_url( $full_size ); ?>" title="<?php echo esc_attr( get_the_title( $medium ) ); ?>" alt="<?php echo esc_attr( get_the_title( $medium ) ); ?>">
                        <?php echo wp_get_attachment_image( $medium, 'gallery-thumbnail', false, array('class' => 'gallery-image') ); ?>
                    </a>
                    <?php
                endforeach;
            endwhile;
            wp_reset_postdata();
            ?>
        </div>
    </div>
</section>
<?php
get_footer();
