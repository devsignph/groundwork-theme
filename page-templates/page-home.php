<?php
/**
* Template Name: Home Page
* The template for displaying the Home page.
*
* @package Groundwork
*/

get_header();
?>
<div id="homeCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#homeCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#homeCarousel" data-slide-to="1"></li>
        <li data-target="#homeCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="https://via.placeholder.com/1366x400" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
                <h5>Image 1</h5>
                <p>This is a sample description for Image 1</p>
            </div>
        </div>
        <div class="carousel-item">
            <img src="https://via.placeholder.com/1366x400" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
                <h5>Image 2</h5>
                <p>This is a sample description for Image 2</p>
            </div>
        </div>
        <div class="carousel-item">
            <img src="https://via.placeholder.com/1366x400" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
                <h5>Image 3</h5>
                <p>This is a sample description for Image 3</p>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#homeCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#homeCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<?php
get_footer();
