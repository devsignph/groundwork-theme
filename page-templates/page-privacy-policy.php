<?php
/**
* Template Name: Privacy Policy Page
* The template for displaying the Privacy Policy page.
*
* @package WordPress
* @subpackage Devsign Base Template
*/

get_header();
if ( have_posts() ) :
    while ( have_posts() ) :
        the_post();
        ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
        <?php
    endwhile;
endif;
get_footer();
